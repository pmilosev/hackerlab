#!/bin/sh

export HL_ROOT="$(dirname $(dirname $(dirname $(realpath "$0"))))"
. $HL_ROOT/common/common.sh

# CONFIG {{{
___SCRIPTDIR=$(dirname "$HL_SCRIPT")
___BOXNAME=$(basename "$___SCRIPTDIR")
___VMNAME="hackerlab-setup-$___BOXNAME"

___IMG_REMOTE="https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.1.0-amd64-netinst.iso"
___IMG_LOCAL="$HL_BUILDDIR/$___BOXNAME/$(basename $___IMG_REMOTE)"
___VMDK="$HL_BUILDDIR/$___BOXNAME/$___VMNAME.vmdk"
___BOXFILE="$HL_BUILDDIR/$___BOXNAME/$___BOXNAME.ova"
# }}}

# DEPENDENCIES {{{
___download_iso() {
    HL_message "Downloading Debian image [$___IMG_REMOTE] ..."

    if [ -s "$___IMG_LOCAL" ]; then
        HL_warning "$(cat <<EOM
Image already seems to be downloaded!
$___IMG_LOCAL

If you continue the local copy will be used.
Kill the script and remove the local image to redownload it.
EOM
)"

        return 0
    fi

    wget -O - "$___IMG_REMOTE" > "$___IMG_LOCAL"
    [ $? -eq 0 ] || HL_error "Could not download the Debian image!"
    HL_cleanup "rm '$___IMG_LOCAL'"
}

___prepare_dependencies() {
    ___download_iso
}
# }}}

# BUILD {{{
___create_vm() {
    HL_message "Creating intermediary VM..."

    VBoxManage createvm  --name "$___VMNAME" --register                                                \
        && VBoxManage modifyvm "$___VMNAME"                                                            \
        --description                                                      "Debian VM."                \
        --ostype                                                           "Debian_64"                 \
        --memory                                                           "2048"                      \
        --boot1                                                            "dvd"                       \
        --boot2                                                            "disk"                      \
        --boot3                                                            "none"                      \
        --boot4                                                            "none"                      \
        --ioapic                                                           "on"                        \
        --x2apic                                                           "on"                        \
        --rtcuseutc                                                        "on"                        \
        --mouse                                                            "usb"                       \
        --cpus                                                             "2"                         \
        --longmode                                                         "on"                        \
        --paravirtprovider                                                 "kvm"                       \
        --pae                                                              "off"                       \
        --vram                                                             "64"                        \
        --graphicscontroller                                               "vmsvga"                    \
        --accelerate3d                                                     "off"                       \
        --audioout                                                         "on"                        \
        --nic1                                                             "nat"                       \
        --nictype1                                                         "virtio"                    \
        --nic2                                                             "null"                      \
        --nictype2                                                         "virtio"                    \
        --natpf1                                                           "ssh,tcp,,2222,,22"         \
        --usbehci                                                          "on"                        \
        && VBoxManage storagectl "$___VMNAME"                                                          \
        --name                                                             "SATA Controller"           \
        --add                                                              "sata"                      \
        --portcount                                                        "1"                         \
        --hostiocache                                                      "on"                        \
        --bootable                                                         "on"                        \
        && VBoxManage createmedium disk --filename "$___VMDK" --size 20480                             \
        && VBoxManage storageattach "$___VMNAME"                                                       \
        --storagectl                                                       "SATA Controller"           \
        --port                                                             "0"                         \
        --type                                                             "hdd"                       \
        --medium "$___VMDK"                                                                            \
        && VBoxManage storagectl "$___VMNAME"                                                          \
        --name                                                             "IDE Controller"            \
        --add                                                              "ide"                       \
        && VBoxManage storageattach "$___VMNAME"                                                       \
        --storagectl                                                       "IDE Controller"            \
        --port                                                             "0"                         \
        --device                                                           "1"                         \
        --type                                                             "dvddrive"                  \
        --medium                                                           $(realpath "$___IMG_LOCAL")

    [ $? -eq 0 ] || HL_error "Failed creating intermediary VM [$___VMNAME]!"
    HL_cleanup "VBoxManage unregistervm --delete '$___VMNAME'"
}

___install_os() {
    HL_message "Installing Debian ..."

    VBoxManage startvm "$___VMNAME"                                                                   && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<dwn>' '<ret>')      && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_LONG     \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_LONG     \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes d e b i a n '<ret>')  && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes d e b i a n '<ret>')  && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes d e b i a n '<ret>')  && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_LONG     \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<tab>' '<ret>')      && sleep $HL_LONGEST  \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<tab>' '<ret>')      && sleep $HL_LONGEST  \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<spc>' '<dwn>')      && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<spc>' '<dwn>' '<dwn>' '<dwn>' '<dwn>' '<dwn>' '<dwn>' '<dwn>' '<dwn>' '<dwn>') && sleep  $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<spc>' '<tab>')      && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_LONGEST  \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')              && sleep $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<dwn>' '<ret>')      && sleep $HL_LONG     \
        && VBoxManage storageattach $___VMNAME                                                                              \
        --storagectl "IDE Controller"                                                                                       \
        --port "0"                                                                                                          \
        --device "1"                                                                                                        \
        --forceunmount                                                                                                      \
        --medium "emptydrive"                                                                                               \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')             && sleep $HL_SHORT     \
        && VBoxManage controlvm "$___VMNAME" poweroff                                                && sleep $HL_LONG

    [ $? -eq 0 ] || HL_error "Failed installing the OS."
}

___provision() {
    HL_message "Initial guest provisioning..."
}

___prepare_vm() {
     ___prepare_dependencies
     ___create_vm
     ___install_os
    ___provision
}
# }}}

# PACKAGE {{{
___package_box() {
    HL_message "Packing [$___VMNAME] into a box [$___BOXFILE] ..."

    if [ -s "$___BOXFILE" ]; then
        HL_warning "$(cat <<EOM
The box seems to already be packaged!

If you continue the existing package will be used.
Kill the script and remove it to repackage the box.
EOM
)"
        return 0
    fi

    mkdir -p $(dirname "$___BOXFILE")

    ___prepare_vm
    VBoxManage export "$___VMNAME" -o "$___BOXFILE"
}
# }}}

___package_box
HL_success "Box have been successfully built!"
