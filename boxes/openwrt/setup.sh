#!/bin/sh

export HL_ROOT="$(dirname $(dirname $(dirname $(realpath "$0"))))"
. $HL_ROOT/common/common.sh || exit 1

# CONFIG {{{
___SCRIPTDIR=$(dirname "$HL_SCRIPT")
___BOXNAME=$(basename "$___SCRIPTDIR")
___VMNAME="hackerlab-setup-$___BOXNAME"

___IMG_REMOTE="https://downloads.openwrt.org/releases/19.07.7/targets/x86/64/openwrt-19.07.7-x86-64-combined-ext4.img.gz"
#___IMG_REMOTE="https://downloads.openwrt.org/releases/18.06.2/targets/x86/64/openwrt-18.06.2-x86-64-combined-ext4.img.gz"
___IMG_LOCAL="$HL_BUILDDIR/$___BOXNAME/$(basename ${___IMG_REMOTE%.gz})"
___VMDK="$HL_BUILDDIR/$___BOXNAME/$___VMNAME.vmdk"
___BOXFILE="$HL_BUILDDIR/$___BOXNAME/$___BOXNAME.ova"
# }}}

# DEPENDENCIES {{{
___download_iso() {
    HL_message "Downloading OpenWRT image [$___IMG_REMOTE] ..."

    if [ -s "$___IMG_LOCAL" ]; then
        HL_warning "$(cat <<EOM
Image already seems to be downloaded!
$___IMG_LOCAL

If you continue the local copy will be used.
Kill the script and remove the local image to redownload it.
EOM
)"

        return 0
    fi

    wget -O - "$___IMG_REMOTE" | gunzip - > "$___IMG_LOCAL"
    [ $? -eq 0 ] || HL_error "Could not download or unpack the OpenWRT image!"
    HL_cleanup "rm '$___IMG_LOCAL'"
}
# }}}

# BUILD {{{
___configure_vm() {
    HL_message "Fixing OpenWRT default networking to match VM's configuration..."

    VBoxManage startvm "$___VMNAME"                                                                                                                             && sleep $HL_LONG     \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')                                                                        && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci add firewall rule") '<ret>')                               && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci set firewall.@rule[-1].name='ACCEPT_SSH_WAN'") '<ret>')    && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci set firewall.@rule[-1].src='wan'") '<ret>')                && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci set firewall.@rule[-1].dest_port='22'") '<ret>')           && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci set firewall.@rule[-1].proto='tcp'") '<ret>')              && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci set firewall.@rule[-1].target='ACCEPT'") '<ret>')          && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')                                                                        && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci set network.lan.ipaddr='${HL_VNET_IP//XXX/1}'") '<ret>')   && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "uci commit") '<ret>')                                          && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')                                                                        && sleep $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes $(HL_texttokeys "service network restart && service firewall restart") '<ret>') && sleep $HL_SHORT

    [ $? -eq 0 ] || HL_error "\tFailed setting lan zone to NAT"

    HL_message "Initial guest provisioning..."
    ssh root@127.0.0.1 -p 2222 -o UserKnownHostsFile=/dev/null -o ConnectTimeout=$HL_SHORT -o StrictHostKeyChecking=no << EOM
echo "\n------------ VM BEGIN -------------\n"

[ $? -ne 0 ] && exit 1 || echo "\nSetting shutdown script ...\n"
[ $? -ne 0 ] && exit 1 || echo "#!/bin/sh" > /bin/shutdown
[ $? -ne 0 ] && exit 1 || echo "exec halt" >> /bin/shutdown
[ $? -ne 0 ] && exit 1 || chmod 744 /bin/shutdown

[ $? -ne 0 ] && exit 1 || echo "\nUsers: Installing needed packages ...\n"
[ $? -ne 0 ] && exit 1 || opkg update
[ $? -ne 0 ] && exit 1 || opkg install sudo shadow-chpasswd shadow-useradd wget libustream-openssl ca-certificates

[ $? -ne 0 ] && exit 1 || echo "\nWIFI: Installing needed packages ...\n"
[ $? -ne 0 ] && exit 1 || opkg install kmod-ath9k-htc hostapd

[ $? -ne 0 ] && exit 1 || echo "\nWIFI: Configuring ...\n"
[ $? -ne 0 ] && exit 1 || (hostapd > /dev/null 2>&1; wifi config) || true

[ $? -ne 0 ] && exit 1 || echo "\nFIREWALL: Allow port 80 (LUCI) on WAN ...\n"
[ $? -ne 0 ] && exit 1 || uci add firewall rule
[ $? -ne 0 ] && exit 1 || uci set firewall.@rule[-1].name="ACCEPT_HTTP_WAN"
[ $? -ne 0 ] && exit 1 || uci set firewall.@rule[-1].src="wan"
[ $? -ne 0 ] && exit 1 || uci set firewall.@rule[-1].dest_port="80"
[ $? -ne 0 ] && exit 1 || uci set firewall.@rule[-1].proto="tcp"
[ $? -ne 0 ] && exit 1 || uci set firewall.@rule[-1].target="ACCEPT"
[ $? -ne 0 ] && exit 1 || uci commit firewall
[ $? -ne 0 ] && exit 1 || /etc/init.d/firewall restart

[ $? -ne 0 ] && exit 1 || echo "\nCleanup & optimization ...\n"
[ $? -ne 0 ] && exit 1 || find /var/log -type f -print | while read log; do dd if=/dev/null of="$log"; done
[ $? -ne 0 ] && exit 1 || (dd if=/dev/zero of=/EMPTY bs=1M 2>/dev/null || echo "")
[ $? -ne 0 ] && exit 1 || rm -f /EMPTY
[ $? -ne 0 ] && exit 1 || sync

[ $? -ne 0 ] && exit 1 || echo "\n------------- VM END  -------------\n"
EOM
    [ $? -eq 0 ] || HL_error "\tFailed setting up the router."
}

___create_vm() {
    HL_message "Creating intermediary VM..."

    VBoxManage createvm  --name "$___VMNAME" --register                         \
        && VBoxManage modifyvm "$___VMNAME"                                     \
        --description "An OpenWRT VM."                                          \
        --ostype "Linux_64"                                                     \
        --memory "128"                                                          \
        --cpus "1"                                                              \
        --vram "16"                                                             \
        --rtcuseutc "on"                                                        \
        --paravirtprovider "kvm"                                                \
        --boot1 "disk"                                                          \
        --boot2 "none" --boot3 "none" --boot4 "none"                            \
        --nic1 "intnet"                                                         \
        --nictype1 "virtio"                                                     \
        --intnet1 "$HL_VNET"                                                    \
        --nic2 "nat"                                                            \
        --nictype2 "virtio"                                                     \
        --natpf2 "ssh,tcp,127.0.0.1,2222,,22"                                   \
        --natpf2 "luci,tcp,127.0.0.1,8080,,80"                                  \
        --audio "none"                                                          \
        --usb "on"                                                              \
        --usbohci "on"                                                          \
        && VBoxManage storagectl "$___VMNAME"                                   \
        --name "SATA Controller"                                                \
        --add "sata"                                                            \
        --portcount "1"                                                         \
        --hostiocache "on"                                                      \
        --bootable "on"                                                         \
        && VBoxManage convertfromraw --format VMDK "$___IMG_LOCAL" "$___VMDK"   \
        && VBoxManage storageattach "$___VMNAME"                                \
        --storagectl "SATA Controller"                                          \
        --port "1"                                                              \
        --type "hdd"                                                            \
        --nonrotational "on"                                                    \
        --medium "$___VMDK"

    [ $? -eq 0 ] || HL_error "Failed creating intermediary VM [$___VMNAME]!"
    HL_cleanup "VBoxManage unregistervm --delete '$___VMNAME'"
}

___prepare_vm() {
    HL_message "Preparing intermediary VM [$___VMNAME] ..."
    VBoxManage showvminfo "$___VMNAME" 2>/dev/null
    if [ $? -eq 0 ]; then
        HL_warning "$(cat <<EOM
VM already seems to be ready (see details above)!

If you continue the existing VM will be used.
Kill the script and remove the VM to recreate it.
EOM
)"
        return 0
    fi

    ___download_iso
    ___create_vm
    ___configure_vm

    VBoxManage controlvm "$___VMNAME" acpipowerbutton && sleep $HL_LONG
    VBoxManage showvminfo "$___VMNAME" | grep "^State:" | grep "powered off" \
        || (HL_message "Giving the VM chance to power down [$vm_state] ..." && sleep HL_LONGEST)

    VBoxManage showvminfo "$___VMNAME" | grep "^State:" | grep "powered off" \
        || HL_warning "$(cat <<EOM
The intermediary VM failed to power off!
Status: [$vm_state]

If this is just a timing issue you can power it off manually and continue.
Otherwise kill this script and investigate.
EOM
)"
}
# }}}

# PACKAGE {{{
___package_box() {
    HL_message "Packing [$___VMNAME] into a box [$___BOXFILE] ..."

    if [ -s "$___BOXFILE" ]; then
        HL_warning "$(cat <<EOM
The box seems to already be packaged!

If you continue the existing package will be used.
Kill the script and remove it to repackage the box.
EOM
)"
        return 0
    fi

    mkdir -p $(dirname "$___BOXFILE")

    ___prepare_vm
    VBoxManage export "$___VMNAME" -o "$___BOXFILE"
}
# }}}

___package_box
HL_success "Box have been successfully built!"
