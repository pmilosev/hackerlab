#!/bin/sh

export HL_ROOT="$(dirname $(dirname $(dirname $(realpath "$0"))))"
. $HL_ROOT/common/common.sh || exit 1

# CONFIG {{{
___BOXNAME=$(basename $(dirname $(realpath "$0")))
___VMNAME="hackerlab-setup-$___BOXNAME"
___SCRIPTDIR=$(dirname "$HL_SCRIPT")
___PROVISION_SCRIPT="$___SCRIPTDIR/provision.sh"

___IMG_REMOTE="https://images.kali.org/virtual-images/kali-linux-2021.1-vbox-amd64.ova"
___IMG_LOCAL="$HL_BUILDDIR/$___BOXNAME/$(basename $___IMG_REMOTE)"
___BOXFILE="$HL_BUILDDIR/$___BOXNAME/$___BOXNAME.ova"
# }}}

# DEPENDENCIES {{{
___download_kali() {
    HL_message "Downloading Kali VBox image [$___IMG_REMOTE] ..."

    if [ -s "$___IMG_LOCAL" ]; then
        HL_warning "$(cat <<EOM
Image already seems to be downloaded!
$___IMG_LOCAL

If you continue the local copy will be used.
Kill the script and remove the local image to redownload it.
EOM
)"

        return 0
    fi

    wget -O - "$___IMG_REMOTE" > "$___IMG_LOCAL"
    [ $? -eq 0 ] || HL_error "\tCould not download Kali VBoximage."
    HL_cleanup "rm '$___IMG_LOCAL'"
}
# }}}

# BUILD {{{
___prepare_vm() {
    HL_message "Preparing intermediary VM [$___VMNAME] ..."
    VBoxManage showvminfo "$___VMNAME" 2>/dev/null
    if [ $? -eq 0 ]; then
        HL_warning "$(cat <<EOM
VM already seems to be ready (see details above)!

If you continue the existing VM will be used.
Kill the script and remove the VM to recreate it.
EOM
)"
        return 0
    fi

    ___download_kali

    VBoxManage import "$___IMG_LOCAL"       \
        --vsys 0 --eula accept              \
        --vsys 0 --vmname "$___VMNAME"      \
        && VBoxManage modifyvm "$___VMNAME" \
        --paravirtprovider "kvm"            \
        --vram "64"                         \
        --nic1 "intnet"                     \
        --nictype1 "virtio"                 \
        --intnet1 "$HL_VNET"                \
        --usb "on"                          \
        --usbehci "on"

    [ $? -eq 0 ] || HL_error "Failed creating intermediary VM [$___VMNAME]!"
    HL_cleanup "VBoxManage unregistervm --delete '$___VMNAME'"
}
# }}}

# PACKAGE {{{
___package_box() {
    HL_message "Packing [$___VMNAME] into a box [$___BOXFILE] ..."

    if [ -s "$___BOXFILE" ]; then
        HL_warning "$(cat <<EOM
The box seems to already be packaged!

If you continue the existing package will be used.
Kill the script and remove it to repackage the box.
EOM
)"
        return 0
    fi

    mkdir -p $(dirname "$___BOXFILE")

    ___prepare_vm
    VBoxManage export "$___VMNAME" -o "$___BOXFILE"
}
# }}}

___package_box
HL_success "Box have been successfully built!"
