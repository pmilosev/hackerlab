#!/bin/sh

export HL_ROOT="$(dirname $(dirname $(dirname $(realpath "$0"))))"
. $HL_ROOT/common/common.sh

# CONFIG {{{
___SCRIPTDIR=$(dirname "$HL_SCRIPT")
___BOXNAME=$(basename "$___SCRIPTDIR")
___VMNAME="hackerlab-setup-$___BOXNAME"

#___IMG_REMOTE="https://osdn.net/dl/android-x86/android-x86-8.1-r2.iso"
___IMG_REMOTE="https://osdn.net/dl/android-x86/android-x86-9.0-r2.iso"
___IMG_LOCAL="$HL_BUILDDIR/$___BOXNAME/$(basename $___IMG_REMOTE)"
___VMDK="$HL_BUILDDIR/$___BOXNAME/$___VMNAME.vmdk"
___BOXFILE="$HL_BUILDDIR/$___BOXNAME/$___BOXNAME.ova"

(uname | grep -i Linux > /dev/null) && os="linux" || os="darwin"
___ADB_REMOTE="https://dl.google.com/android/repository/platform-tools-latest-$os.zip"
___ADB_LOCAL="$HL_BUILDDIR/$___BOXNAME/$(basename $___ADB_REMOTE)"
___ADB=$(which adb) || ___ADB="$(dirname "$___ADB_LOCAL")/platform-tools/adb"

___WIFICONF="$___SCRIPTDIR/WifiConfigStore.xml"

___SSHD_REMOTE="http://www.galexander.org/software/simplesshd/SimpleSSHD-27.apk"
___SSHD_LOCAL="$HL_BUILDDIR/$___BOXNAME/$(basename $___SSHD_REMOTE)"
___SSHD_PREF="$___SCRIPTDIR/org.galexander.sshd_preferences.xml"
# }}}

# DEPENDENCIES {{{
___download_iso() {
    HL_message "Downloading Androidx86 image [$___IMG_REMOTE] ..."

    if [ -s "$___IMG_LOCAL" ]; then
        HL_warning "$(cat <<EOM
Image already seems to be downloaded!
$___IMG_LOCAL

If you continue the local copy will be used.
Kill the script and remove the local image to redownload it.
EOM
)"

        return 0
    fi

    wget -O - "$___IMG_REMOTE" > "$___IMG_LOCAL"
    [ $? -eq 0 ] || HL_error "Could not download the ANDROIDx86 image!"
    HL_cleanup "rm '$___IMG_LOCAL'"
}

___download_adb() {
    HL_message "Downloading ADB image [$___ADB_REMOTE] ..."

    if [ -s "$___ADB_LOCAL" ]; then
        HL_warning "$(cat <<EOM
ADB already seems to be downloaded!
$___ADB_LOCAL

If you continue the local copy will be used.
Kill the script and remove the local version to redownload it.
EOM
)"

        return 0
    fi

    wget -O - "$___ADB_REMOTE" > "$___ADB_LOCAL"              \
        && unzip "$___ADB_LOCAL" -d $(dirname $___ADB_LOCAL)/

    [ $? -eq 0 ] || HL_error "Could not download ADB!"
    HL_cleanup "rm '$___ADB_LOCAL'"
    HL_cleanup "rm -rf $(dirname "$___ADB_LOCAL")"
}

___download_sshd() {
    HL_message "Downloading SSHD [$___SSHD_REMOTE] ..."

    if [ -s "$___SSHD_LOCAL" ]; then
        HL_warning "$(cat <<EOM
SSHD already seems to be downloaded!
$___SSHD_LOCAL

If you continue the local copy will be used.
Kill the script and remove the local image to redownload it.
EOM
)"

        return 0
    fi

    wget -O - "$___SSHD_REMOTE" > "$___SSHD_LOCAL"
    [ $? -eq 0 ] || HL_error "Could not download SSHD!"
    HL_cleanup "rm '$___SSHD_LOCAL'"
}

___prepare_dependencies() {
    ___download_iso
    ___download_sshd

    "$___ADB" --version > /dev/null
    [ $? -eq 0 ] || ___download_adb
}
# }}}

# BUILD {{{
___create_vm() {
    HL_message "Creating intermediary VM..."

    VBoxManage createvm  --name "$___VMNAME" --register                                                \
        && VBoxManage modifyvm "$___VMNAME"                                                            \
        --description                                                      "Android-X86 VM."           \
        --ostype                                                           "Linux26"                   \
        --memory                                                           "2048"                      \
        --boot1                                                            "dvd"                       \
        --boot2                                                            "disk"                      \
        --boot3                                                            "none"                      \
        --boot4                                                            "none"                      \
        --ioapic                                                           "on"                        \
        --x2apic                                                           "on"                        \
        --rtcuseutc                                                        "on"                        \
        --mouse usbmultitouch                                                                          \
        --cpus                                                             "2"                         \
        --longmode                                                         "off"                       \
        --paravirtprovider                                                 "kvm"                       \
        --pae                                                              "off"                       \
        --vram                                                             "64"                        \
        --graphicscontroller                                               "vboxvga"                   \
        --accelerate3d                                                     "off"                       \
        --audioout                                                         "on"                        \
        --nic1                                                             "null"                      \
        --nictype1                                                         "virtio"                    \
        --nic2                                                             "nat"                       \
        --nictype2                                                         "virtio"                    \
        --natpf2                                                           "ssh,tcp,,2222,,2222"       \
        --natpf2                                                           "adb,tcp,,5555,,5555"       \
        --usbehci                                                          "on"                        \
        && VBoxManage storagectl "$___VMNAME"                                                          \
        --name                                                             "SATA Controller"           \
        --add                                                              "sata"                      \
        --portcount                                                        "1"                         \
        --hostiocache                                                      "on"                        \
        --bootable                                                         "on"                        \
        && VBoxManage createmedium disk --filename "$___VMDK" --size 20480                             \
        && VBoxManage storageattach "$___VMNAME"                                                       \
        --storagectl                                                       "SATA Controller"           \
        --port                                                             "0"                         \
        --type                                                             "hdd"                       \
        --medium "$___VMDK"                                                                            \
        && VBoxManage storagectl "$___VMNAME"                                                          \
        --name                                                             "IDE Controller"            \
        --add                                                              "ide"                       \
        && VBoxManage storageattach "$___VMNAME"                                                       \
        --storagectl                                                       "IDE Controller"            \
        --port                                                             "0"                         \
        --device                                                           "1"                         \
        --type                                                             "dvddrive"                  \
        --medium                                                           $(realpath "$___IMG_LOCAL")


    [ $? -eq 0 ] || HL_error "Failed creating intermediary VM [$___VMNAME]!"
    HL_cleanup "VBoxManage unregistervm --delete '$___VMNAME'"
}

___install_os() {
    HL_message "Installing the AndroidX86 OS..."

    VBoxManage startvm "$___VMNAME"                                                                      && sleep  $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<dwn>' '<dwn>' '<ret>') && sleep  $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes c '<ret>')               && sleep  $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes n)                       && sleep  $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes n p '<ret>')             && sleep  $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes b W y e s '<ret>')       && sleep  $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes q)                       && sleep  $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')                 && sleep  $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes e '<ret>')               && sleep  $HL_SHORTEST \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes y)                       && sleep  $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes y)                       && sleep  $HL_SHORT    \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes y)                       && sleep  $HL_LONGEST  \
        && VBoxManage storageattach $___VMNAME                                                                                  \
        --storagectl "IDE Controller"                                                                                           \
        --port "0"                                                                                                              \
        --device "1"                                                                                                            \
        --forceunmount                                                                                                          \
        --medium "emptydrive"                                                                                                   \
        && VBoxManage controlvm "$___VMNAME" keyboardputscancode $(HL_scancodes '<ret>')

    [ $? -eq 0 ] || HL_error "Failed installing the OS."
}

___provision() {
    HL_message "Provisioning over ADB..."

    device_status=''
    strikes=5
    while [ "$device_status" != 'device' ]; do
        [ "$strikes" -gt 0 ] || HL_error "Failed connecting the VM to ADB!"

        HL_message "Connecting ADB [$strikes]..."
        [ -z "$device_status" ] && ("$___ADB" kill-server && "$___ADB" start-server)

        count=$("$___ADB" devices | grep -v '^List of devices\|^[ 	]*$' | wc -l)
        [ "$count" -le 1 ] || HL_warning "$(cat <<EOM
There are devices other than the VM detected by ADB!
Please detach all devices except the VM before continuing.
EOM
)"

        device_status=$($___ADB devices | grep '^emulator-' | xargs | cut -d' ' -f2)
        [ "$device_status" != "device" ]                                      \
            && HL_message "VM not ready yet - will try again in a while. zzZ" \
            && sleep $HL_LONG

        strikes=$(( strikes - 1 ))
    done

    HL_message "Faking missing commands..."
    HL_message "- sudo"
    ("$___ADB" push "$___SCRIPTDIR"/sudo /sdcard/sudo)                          \
        && ("$___ADB" shell su -c "'mv -f /sdcard/sudo /system/bin/sudo'" root) \
        && ("$___ADB" shell su -c "'chmod 755 /system/bin/sudo'" root)

    [ $? -eq 0 ] || HL_error "Failed faking sudo!"

    HL_message "- shutdown"
    ("$___ADB" push "$___SCRIPTDIR"/shutdown /sdcard/shutdown)                \
        && ("$___ADB" shell sudo mv -f /sdcard/shutdown /system/bin/shutdown) \
        && ("$___ADB" shell sudo chmod 755 /system/bin/shutdown)

    [ $? -eq 0 ] || HL_error "Failed faking shutdown!"

    HL_message "Installing SimpleSSHD..."
    ("$___ADB" install "$___SSHD_LOCAL")
    [ $? -eq 0 ] || HL_error "Failed installing the SSH server!"

    HL_message "Configuring SimpleSSHD..."
    sudb='/data/user_de/0/com.android.settings/databases/su.sqlite'
    package='org.galexander.sshd'
    sshd_data="/data/data/$package"

    [ $? -eq 0 ] && uname=$("$___ADB" shell sudo stat -c '%U' $sshd_data)
    [ $? -eq 0 ] && gname=$("$___ADB" shell sudo stat -c '%g' $sshd_data)
    [ $? -eq 0 ] && anuid=$("$___ADB" shell sudo stat -c '%u' $sshd_data)
    [ $? -eq 0 ] || HL_error "Failed retrieving system info!"

    HL_message "- shared preferences"
    ("$___ADB" shell monkey -p "$package" 1 && sleep $HL_SHORT)                                        \
        && ("$___ADB" push "$___SSHD_PREF" /sdcard/)                                                   \
        && ("$___ADB" shell sudo mv -f /sdcard/$(basename "$___SSHD_PREF") "$sshd_data"/shared_prefs/) \
        && ("$___ADB" shell sudo chown -R $uname:$gname "$sshd_data"/shared_prefs)

    [ $? -eq 0 ] || HL_error "Failed installing SimpleSSHD preferences!"

    HL_message "- root access"
    "$___ADB" shell su -c "'echo \"INSERT INTO uid_policy (logging, policy, until, command, uid, desired_uid, package_name, name, notification) VALUES (1, \\\"allow\\\", 0, \\\"\\\", $anuid, 0, \\\"$package\\\", \\\"\\\", 1);\" | sqlite3 $sudb'" root

    [ $? -eq 0 ] || HL_warning "Users over SSH will not be able to gain root access!"

    HL_message "Setting Android screen off timeout to 5 days..."
    "$___ADB" shell su -c "'$(cat <<EOM
ed /data/system/users/0/settings_system.xml <<MOE
/screen_off_timeout/
a
<setting id="47" name="screen_off_timeout" value="432000000" package="com.android.settings" defaultValue="432000000" defaultSysSet="true" />
.
-1
d
w
q
MOE
EOM
)'" root

    [ $? -eq 0 ] || HL_warning "Failed setting screen timeout!"

    HL_message "Provisioning done! Shutting down the VM..."
    ("$___ADB" shell sudo sync) \
        && ("$___ADB" shell sudo shutdown)

    sleep $HL_SHORT
}

___prepare_vm() {
    ___prepare_dependencies
    ___create_vm
    ___install_os
    ___provision
}
# }}}

# PACKAGE {{{
___package_box() {
    HL_message "Packing [$___VMNAME] into a box [$___BOXFILE] ..."

    if [ -s "$___BOXFILE" ]; then
        HL_warning "$(cat <<EOM
The box seems to already be packaged!

If you continue the existing package will be used.
Kill the script and remove it to repackage the box.
EOM
)"
        return 0
    fi

    mkdir -p $(dirname "$___BOXFILE")

    ___prepare_vm
    VBoxManage export "$___VMNAME" -o "$___BOXFILE"
}
# }}}

___package_box
HL_success "Box have been successfully built!"
