#!/bin/sh

# Usage:
#   $ init.sh <hackerlab root path> [project path]

export HL_ROOT=$(realpath "$1")
[ ! -d "$HL_ROOT" ] && echo ">> ERROR [$0]:\nInvalid path to HackerLab's root directory!\n" && exit 1
. "$HL_ROOT"/common/common.sh || exit 1

# If no second argument is provided then the current directory (PWD) is condiered the project
shift && ___PRJDIR="$1" || ___PRJDIR="$HL_PROJECT"
___PRJDIR=$(realpath "$___PRJDIR")

# CONFIG {{{
# You can manually set (hardcode) these values to influence how the overal lab setup looks like
___REPO=${HL_REPO:?Must be set}                                 # The repository to load the boxes from
___PRJNAME=${___PRJDIR##"$HOME/"}                               # Used as prefix for VMs, etc.
___PRJNAME=${___PRJNAME//\//-}                                  # -------------------------------
___VNET="$___PRJNAME-hackernet"                                 # Lab's virtual lan name
___UNIQUE=$(echo $___PRJDIR | md5sum | sed -e 's/[^0-9]//g');   # Semi-unique value for MACs and IPs

# Network config templates
___UNIQUE=$(( ___UNIQUE % 4294967295 ))
___VNET_MAC="$(printf '42XX%08X' $___UNIQUE | sed -e 's/\(..\)/\1:/g' | cut -c-17)"
___VNET_PORT="$((___UNIQUE % 45 ))XYY"
___VNET_IP="192.168.$(( 128 + ___UNIQUE % 127 )).XXX"
# }}}

# VM IMPORT & COMMON SETUP {{{
# If you edit some of the naming patterns here,
# you will probably break the rest of th script!
#
# Assumptions:
#   - The name and the order of the VMs
#   - Neither the name of the VMs nor the boxes contain spaces or the used config separator
#
# Config convention:
# VM :: BOX
___VMS=$(cat <<EOM | tr -d ' '
gate      :: openwrt
hackerbox :: kali
ax86      :: androidx86
server    :: server
EOM
)

HL_message "Instantiating boxes..."
count=0
for vm in $___VMS; do
    count=$(( count + 1 ))
    vmname="$___PRJNAME-$(printf '%02d' $count)-${vm%%:*}"
    box="$___REPO/${vm##*:}.ova"
    mac=${___VNET_MAC//XX/$(printf '%02d' $count)}
    mac=${mac//:/}

    HL_message "Instantiating [$vmname]..."
    VBoxManage import           "$box"                                       \
        --vsys 0 --vmname       "$vmname"                                    \
        --vsys 0 --group        "/$___PRJNAME"                               \
        --vsys 0 --settingsfile "$___PRJDIR/.hackerlab/$vmname/$vmname.vbox" \
        --vsys 0 --basefolder   "$___PRJDIR/.hackerlab/"                     \
        || HL_error "Failed importing [$vmname]!"

    HL_message "Common configuration [vmname]..."
    VBoxManage modifyvm "$vmname"                                            \
        --nic1              "intnet"                                         \
        --nictype1          "virtio"                                         \
        --intnet1           "$___VNET"                                       \
        --macaddress1       "$mac"                                           \
        || HL_error "Failed configuring [$vmname]!"

    echo
done
# }}}

# EXTRA SETUP {{{
# Unique configuration for each machine
# NOTE: Heavily makes assumptions about the common setup!

# NOTE: Android-x86 uses nic1 for WIFI emulation.
# It's just easier to have that interface disabled and setup ethernet lan on nic2
vmname="$___PRJNAME-03-ax86"
mac=${___VNET_MAC//XX/03}
mac=${mac//:/}
HL_message "Extra configuration [$vmname]..."
VBoxManage modifyvm "$vmname"                   \
    --nic1          "null"                      \
    --nic2          "intnet"                    \
    --nictype2      "virtio"                    \
    --intnet2       "$___VNET"                  \
    --macaddress2   "$mac"                      \
    --natpf2 delete "ssh"                       \
    --natpf2 delete "adb"                       \
    || HL_error "Failed configuring [$vmname]!"

vmname="$___PRJNAME-01-gate"
HL_message "Extra configuration [$vmname]..."
VBoxManage modifyvm "$vmname"                                                                 \
    --natpf2 delete "ssh"                                                                     \
    --natpf2 delete "luci"                                                                    \
    --natpf2 "gate-luci,tcp,127.0.0.1,${___VNET_PORT//XYY/180},,80"                           \
    --natpf2 "gate-ssh,tcp,127.0.0.1,${___VNET_PORT//XYY/122},,22"                            \
    --natpf2 "hackerbox-ssh,tcp,127.0.0.1,${___VNET_PORT//XYY/222},,${___VNET_PORT//XYY/222}" \
    --natpf2 "ax86-ssh,tcp,127.0.0.1,${___VNET_PORT//XYY/322},,${___VNET_PORT//XYY/322}"      \
    --natpf2 "ax86-adb,tcp,127.0.0.1,${___VNET_PORT//XYY/355},,${___VNET_PORT//XYY/355}"      \
    --natpf2 "server-ssh,tcp,127.0.0.1,${___VNET_PORT//XYY/422},,${___VNET_PORT//XYY/422}"    \
    --nicpromisc1 allow-all                                                                   \
    || HL_error "Failed configuring [$vmname]!"

HL_message "Provisioning [$vmname]..."
VBoxManage startvm "$vmname"           \
    && sleep $HL_LONG                  \
    && ssh root@127.0.0.1              \
    -p ${___VNET_PORT//XYY/122}        \
    -o ConnectTimeout=$HL_SHORT        \
    -o UserKnownHostsFile=/dev/null    \
    -o StrictHostKeyChecking=no <<EOM

echo "Setting new IP address for the GATE"
[ $? -ne 0 ] && exit 1 || uci set network.lan.ipaddr="${___VNET_IP//XXX/1}"
[ $? -ne 0 ] && exit 1 || uci commit network

[ $? -ne 0 ] && exit 1 || echo "Setting static DHCP lease for HACKERBOX"
[ $? -ne 0 ] && exit 1 || uci add dhcp host
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].name="hackerbox"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].mac="${___VNET_MAC//XX/02}"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].ip="${___VNET_IP//XXX/2}"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].dns='1'
[ $? -ne 0 ] && exit 1 || uci commit dhcp

[ $? -ne 0 ] && exit 1 || echo "Setting static DHCP lease for ANDROIDx86"
[ $? -ne 0 ] && exit 1 || uci add dhcp host
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].name="ax86"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].mac="${___VNET_MAC//XX/03}"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].ip="${___VNET_IP//XXX/3}"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].dns='1'
[ $? -ne 0 ] && exit 1 || uci commit dhcp

[ $? -ne 0 ] && exit 1 || echo "Setting static DHCP lease for SERVER"
[ $? -ne 0 ] && exit 1 || uci add dhcp host
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].name="server"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].mac="${___VNET_MAC//XX/04}"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].ip="${___VNET_IP//XXX/4}"
[ $? -ne 0 ] && exit 1 || uci set dhcp.@host[-1].dns='1'
[ $? -ne 0 ] && exit 1 || uci commit dhcp

[ $? -ne 0 ] && exit 1 || echo "Forward SSH port for HACKERBOX"
[ $? -ne 0 ] && exit 1 || uci add firewall redirect
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].name="hackerbox_ssh"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].target='DNAT'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src='wan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest='lan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_ip="${___VNET_IP//XXX/2}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src_dport="${___VNET_PORT//XYY/222}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_port="22"
[ $? -ne 0 ] && exit 1 || uci commit firewall

[ $? -ne 0 ] && exit 1 || echo "Forward SSH port for ANDROIDx86"
[ $? -ne 0 ] && exit 1 || uci add firewall redirect
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].name="ax86_ssh"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].target='DNAT'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src='wan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest='lan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_ip="${___VNET_IP//XXX/3}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src_dport="${___VNET_PORT//XYY/322}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_port="2222"
[ $? -ne 0 ] && exit 1 || uci commit firewall

[ $? -ne 0 ] && exit 1 || echo "Forward ADB port for ANDROIDx86"
[ $? -ne 0 ] && exit 1 || uci add firewall redirect
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].name="ax86_adb"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].target='DNAT'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src='wan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest='lan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_ip="${___VNET_IP//XXX/3}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src_dport="${___VNET_PORT//XYY/355}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_port="5555"
[ $? -ne 0 ] && exit 1 || uci commit firewall

[ $? -ne 0 ] && exit 1 || echo "Forward SSH port for SERVER"
[ $? -ne 0 ] && exit 1 || uci add firewall redirect
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].name="server"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].target='DNAT'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src='wan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest='lan'
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_ip="${___VNET_IP//XXX/4}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].src_dport="${___VNET_PORT//XYY/422}"
[ $? -ne 0 ] && exit 1 || uci set firewall.@redirect[-1].dest_port="22"
[ $? -ne 0 ] && exit 1 || uci commit firewall

[ $? -ne 0 ] && exit 1 || echo "Set ROOT password to TOOR"
[ $? -ne 0 ] && exit 1 || (echo "root:toor" | chpasswd)
EOM

[ $? -eq 0 ] || HL_error "Failed provisioning [$vmname]!"
VBoxManage controlvm "$vmname" acpipowerbutton && sleep $HL_LONG
# }}}

HL_success "Lab instantiated successfully!"

# TODO: stacked clenup
# TODO: imports - too much code duplication
# TODO: NAT on first interface ITNET on second (if first can be disabled for hackerbox) - due to AX86 managing WIFI on int1
