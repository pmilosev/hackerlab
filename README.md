```
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          _  _         _           _         _
         | || |__ _ __| |_____ _ _| |   __ _| |__
         | __ / _` / _| / / -_) '_| |__/ _` | '_ \
         |_||_\__,_\__|_\_\___|_| |____\__,_|_.__/

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
```

A set of scripts to help bootstrapping a security research & testing lab.

## Principles

- Depend heavily on VirtualBox.
- Have a set of base boxes which can be used to build more complex setups.
- Have a set of templates that describe such setups.
- Once built the base boxes are kept in a repository as OVA packages.
- When instantiating a template a lab is created in the current directory.
  In practice a set of VMs is created by importing the base boxes.

## Intended usage
- Init a new lab per research or pen-testing assignment.
- Destroy the lab afterwards.

`$ hackerlab -h`
