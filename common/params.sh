# Delays
export HL_SHORTEST=2
export HL_SHORT=10
export HL_LONG=30
export HL_LONGEST=90

# PATHS
export HL_PROJECT="$PWD"
export HL_SCRIPT=$(realpath "$0")
export HL_BOXDIR="${HL_ROOT:?Must be set}/boxes"
export HL_BUILDDIR="${HL_ROOT:?Must be set}/build"
export HL_TEMPLATESDIR="${HL_ROOT:?Must be set}/templates"

HL_REPO="$HOME/.local/share/hackerlab"
[ -d "$(dirname $HL_REPO)" ] || HL_REPO="$HOME/.hackerlab"
export HL_REPO

# NETWORK
HL_VNET="hackernet"
HL_VNET_IP="192.168.127.XXX"
