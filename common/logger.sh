___SCRIPT="${HL_SCRIPT:?Currently running script shuold be known!!!}"
___FIRST=0

HL_message() {
    [ $___FIRST -eq 0 ] && cat >&2 <<EOM

------------------------------------------------------------
$(if [ ${#___SCRIPT} -gt 60 ]; then
head="$(echo "$___SCRIPT" | cut -c-15)"
tail="$(echo "$___SCRIPT" | rev | cut -c-40 | rev)"
___SCRIPT="$head ... $tail"
fi

echo "$___SCRIPT")
------------------------------------------------------------

EOM
    ___FIRST=1
    printf ">> $@\n\n" >&2
}

HL_warning() {
    HL_message "WARNING\n$1\n"
    read -n 1 -p "[press any key to continue or Ctrl+C to kill the script]"
}

HL_error() {
    HL_message "ERROR\n$1\n"
    [ -n "$2" ] || exit 1
    exit $2
}

HL_success() {
    HL_message "SUCCESS\n$1\n"
    exit 0
}

HL_hello() {
cat <<"EOM"

<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
          _  _         _           _         _
         | || |__ _ __| |_____ _ _| |   __ _| |__
         | __ / _` / _| / / -_) '_| |__/ _` | '_ \
         |_||_\__,_\__|_\_\___|_| |____\__,_|_.__/

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

EOM
}
