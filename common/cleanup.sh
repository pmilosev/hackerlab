################### CLEANUP ######################
# For better context, we will store the cleanup commands in this array
# and have them executed automatically once everything is done


___CLEANUPCMDS=''
___INTERRUPTED=0

trap ___cleanup 0
trap ___interrupted 2

___interrupted() {
    ___INTERRUPTED=127
    exit $___INTERRUPTED
}

___cleanup() {
    [ $___INTERRUPTED -ne 0 ] && exit $___INTERRUPTED

    if [ -n "$___CLEANUPCMDS" ]; then
        HL_warning "$(cat <<EOF
Following cleanup steps are about to be performed:
$(echo "$___CLEANUPCMDS" | tr ';' '\n' | grep '.' | sed 's/^/    $ /')

If you want to preserve something, either make a copy or kill this script.
EOF
)"
        ___IFSBACK="$IFS"; IFS=";"
        for ___CMD in $___CLEANUPCMDS; do
            HL_message "$ $___CMD"
            eval "$___CMD"
        done
        IFS="$___IFSBACK"
    fi
}

HL_cleanup () {
    ___CLEANUPCMDS="$@;$___CLEANUPCMDS"
}
