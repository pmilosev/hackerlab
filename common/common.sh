___ROOT=${HL_ROOT:?Calling script must resolve path to HackerLab\'s base dir!!!}

. $___ROOT/common/params.sh    || exit 1
. $___ROOT/common/logger.sh    || exit 1
. $___ROOT/common/cleanup.sh   || exit 1
. $___ROOT/common/scancodes.sh || exit 1
