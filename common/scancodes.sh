#!/bin/sh

HL_texttokeys () {
  ___KEYS=""

  while [ "$#" -gt 0 ]; do
    ___KEYS="$___KEYS <spc> $(echo $1 | sed 's/\(.\)/\1 /g' | sed 's/  / <spc> /g')"
    shift
  done

  # There is an extra ' <spc> ' before the first key - remove it
  ___KEYS=${___KEYS# <spc> }

  echo "$___KEYS"
}

HL_scancodes () {
  ___CODES=""

  HL_message "Sending scancodes for:" $@
  while [ "$#" -gt 0 ]; do
    ___CODES="$___CODES $(___code $1)"
    shift
  done

  echo "$___CODES"
}

___code () {
  case "$1" in
    "<esc>")
      echo "01 81"
      ;;
    "1")
      echo "02 82"
      ;;
    "!")
      echo "2a 02 82 aa"
      ;;
    "2")
      echo "03 83"
      ;;
    "@")
      echo "2a 03 83 aa"
      ;;
    "3")
      echo "04 84"
      ;;
    "#")
      echo "2a 04 84 aa"
      ;;
    "4")
      echo "05 85"
      ;;
    "$")
      echo "2a 05 85 aa"
      ;;
    "5")
      echo "06 86"
      ;;
    "%")
      echo "2a 06 86 aa"
      ;;
    "6")
      echo "07 87"
      ;;
    "^")
      echo "2a 07 87 aa"
      ;;
    "7")
      echo "08 88"
      ;;
    "&")
      echo "2a 08 88 aa"
      ;;
    "8")
      echo "09 89"
      ;;
    "*")
      echo "2a 09 89 aa"
      ;;
    "9")
      echo "0a 8a"
      ;;
    "(")
      echo "2a 0a 8a aa"
      ;;
    "0")
      echo "0b 8b"
      ;;
    ")")
      echo "2a 0b 8b aa"
      ;;
    "-")
      echo "0c 8c"
      ;;
    "_")
      echo "2a 0c 8c aa"
      ;;
    "=")
      echo "0d 8d"
      ;;
    "+")
      echo "2a 0d 8d aa"
      ;;
    "<bsp>")
      echo "0e 8e"
      ;;
    "<tab>")
      echo "0f 8f"
      ;;
    "q")
      echo "10 90"
      ;;
    "Q")
      echo "2a 10 90 aa"
      ;;
    "w")
      echo "11 91"
      ;;
    "W")
      echo "2a 11 91 aa"
      ;;
    "e")
      echo "12 92"
      ;;
    "E")
      echo "2a 12 92 aa"
      ;;
    "r")
      echo "13 93"
      ;;
    "R")
      echo "2a 13 93 aa"
      ;;
    "t")
      echo "14 94"
      ;;
    "T")
      echo "2a 14 94 aa"
      ;;
    "y")
      echo "15 95"
      ;;
    "Y")
      echo "2a 15 95 aa"
      ;;
    "u")
      echo "16 96"
      ;;
    "U")
      echo "2a 16 96 aa"
      ;;
    "i")
      echo "17 97"
      ;;
    "I")
      echo "2a 17 97 aa"
      ;;
    "o")
      echo "18 98"
      ;;
    "O")
      echo "2a 18 98 aa"
      ;;
    "p")
      echo "19 99"
      ;;
    "P")
      echo "2a 19 99 aa"
      ;;
    "{")
      echo "2a 1a 9a aa"
      ;;
    "[")
      echo "1a 9a"
      ;;
    "}")
      echo "2a 1b 9b aa"
      ;;
    "]")
      echo "1b 9b"
      ;;
    "<ret>")
      echo "1c 9c"
      ;;
    "a")
      echo "1e 9e"
      ;;
    "A")
      echo "2a 1e 9e aa"
      ;;
    "s")
      echo "1f 9f"
      ;;
    "S")
      echo "2a 1f 9f aa"
      ;;
    "d")
      echo "20 a0"
      ;;
    "D")
      echo "2a 20 a0 aa"
      ;;
    "f")
      echo "21 a1"
      ;;
    "F")
      echo "2a 21 a1 aa"
      ;;
    "g")
      echo "22 a2"
      ;;
    "G")
      echo "2a 22 a2 aa"
      ;;
    "h")
      echo "23 a3"
      ;;
    "H")
      echo "2a 23 a3 aa"
      ;;
    "j")
      echo "24 a4"
      ;;
    "J")
      echo "2a 24 a4 aa"
      ;;
    "k")
      echo "25 a5"
      ;;
    "K")
      echo "2a 25 a5 aa"
      ;;
    "l")
      echo "26 a6"
      ;;
    "L")
      echo "2a 26 a6 aa"
      ;;
    ":")
      echo "2a 27 a7 aa"
      ;;
    ";")
      echo "27 a7"
      ;;
    "\"")
      echo "2a 28 a8 aa"
      ;;
    "'")
      echo "28 a8"
      ;;
    "~")
      echo "2a 29 a9 aa"
      ;;
    "\`")
      echo "29 a9"
      ;;
    "<shf>")
      echo "2a aa"
      ;;
    "|")
      echo "2a 2b ab aa"
      ;;
    "\\")
      echo "2b ab"
      ;;
    "z")
      echo "2c ac"
      ;;
    "Z")
      echo "2a 2c ac aa"
      ;;
    "x")
      echo "2d ad"
      ;;
    "X")
      echo "2a 2d ad aa"
      ;;
    "c")
      echo "2e ae"
      ;;
    "C")
      echo "2a 2e ae aa"
      ;;
    "v")
      echo "2f af"
      ;;
    "V")
      echo "2a 2f af aa"
      ;;
    "b")
      echo "30 b0"
      ;;
    "B")
      echo "2a 30 b0 aa"
      ;;
    "n")
      echo "31 b1"
      ;;
    "N")
      echo "2a 31 b1 aa"
      ;;
    "m")
      echo "32 b2"
      ;;
    "M")
      echo "2a 32 b2 aa"
      ;;
    "<")
      echo "2a 33 b3 aa"
      ;;
    ",")
      echo "33 b3"
      ;;
    ">")
      echo "2a 34 b4 aa"
      ;;
    ".")
      echo "34 b4"
      ;;
    "?")
      echo "2a 35 b5 aa"
      ;;
    "/")
      echo "35 b5"
      ;;
    "<spc>")
      echo "39 b9"
      ;;
    "<f1>")
      echo "3b bb"
      ;;
    "<f2>")
      echo "3c bc"
      ;;
    "<f3>")
      echo "3d bd"
      ;;
    "<f4>")
      echo "3e be"
      ;;
    "<f5>")
      echo "3f bf"
      ;;
    "<f6>")
      echo "40 c0"
      ;;
    "<f7>")
      echo "41 c1"
      ;;
    "<f8>")
      echo "42 c2"
      ;;
    "<f9>")
      echo "43 c3"
      ;;
    "<f10>")
      echo "44 c4"
      ;;
    "<up>")
      echo "48 c8"
      ;;
    "<lft>")
      echo "4b cb"
      ;;
    "<rgh>")
      echo "4d cd"
      ;;
    "<dwn>")
      echo "50 d0"
      ;;
    *)
      error "Key '$1' has no mapping scancode!"
      ;;
  esac
}
